<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $connect_with_user
 * @property string $name
 * @property string $price
 * @property int $start_date
 * @property int $delivery_date
 *
 * @property User $connectWithUser
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $username;

    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['connect_with_user', 'name', 'price', 'start_date', 'delivery_date'], 'required'],
            [['connect_with_user'], 'integer'],
            [['name', 'price'], 'string', 'max' => 255],
            [['connect_with_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['connect_with_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'connect_with_user' => 'Connect With User',
            'name' => 'Name',
            'price' => 'Price',
            'start_date' => 'Start Date',
            'delivery_date' => 'Delivery Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectWithUser()
    {
        return $this->hasOne(User::className(), ['id' => 'connect_with_user']);
    }

    public  function beforeSave($insert)
    {

        if (\DateTime::createFromFormat('d-m-y', $this -> start_date) !== FALSE && \DateTime::createFromFormat('d-m-y', $this -> delivery_date) !== FALSE) {
            // it's a date
            $this -> start_date =  strtotime($this -> start_date);
            $this -> delivery_date =  strtotime($this -> delivery_date);
            return true;
        } else {
            return false;
        }
    }

//    public function validateDate($date, $format = 'dd-mm-yy')
//    {
//        $d = \DateTime::createFromFormat($format, $date);
//        print_r($d); die();
//        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
//        return $d && $d->format($format) === $date;
//    }
}
