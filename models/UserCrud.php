<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $logs
 * @property string $auth_key
 * @property string $password_hash
 *
 * @property Projects[] $projects
 */
class UserCrud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $password;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'logs', 'password_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 20],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'logs' => 'Logs',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['connect_with_user' => 'id']);
    }
    public  function beforeSave($insert)
    {

        $this -> password_hash = Yii::$app->security->generatePasswordHash($this -> password);
        $this -> auth_key = Yii::$app->security->generateRandomString();
        return true;
    }
}
