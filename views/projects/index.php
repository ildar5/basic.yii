<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Projects', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'connect_with_user',
            'username',
            'name',
            'price',
//            'start_date',
//            'delivery_date',
            [

                'attribute' => 'start_date',

                'format' => ['date', 'php:d/m/Y']

            ],
            [

                'attribute' => 'delivery_date',

                'format' => ['date', 'php:d/m/Y']

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
