<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'connect_with_user')->textInput() ?>

    <?php $users = \yii\helpers\ArrayHelper::map(\app\models\User::find() -> all(), 'id', 'username') ?>

    <?= $form->field($model, 'connect_with_user')->dropDownList($users) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'start_date')->textInput() ?>
<!--    --><?//= $form->field($model, 'delivery_date')->textInput() ?>

    <?php
        // Usage with model and Active Form (with no default initial value)
        echo $form->field($model, 'start_date')->widget(DatePicker::classname(), [
//            'value' => date('d-M-Y', strtotime('now')),
            'options' => ['placeholder' => 'enter date ...'],
            'pluginOptions' => [
                'format' => 'dd-mm-yy',
                'autoclose'=>true,
                'todayHighlight' => true
            ]
        ]);
    ?>

    <?php
    echo $form->field($model, 'delivery_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'enter date ...'],
        'pluginOptions' => [
            'format' => 'dd-mm-yy',
            'autoclose'=>true,
            'todayHighlight' => true
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
