<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserCrud */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-crud-create">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="container">
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
            <div class="col-xs-3"></div>
        </div>
    </div>

</div>
