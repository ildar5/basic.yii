<?php

use yii\db\Migration;

/**
 * Class m181014_094600_projects
 */
class m181014_094600_projects extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'connect_with_user' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'price' => $this->string(255)->notNull(),
            'start_date' => $this->integer()->notNull(),
            'delivery_date' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-connect_with_user',
            'projects',
            'connect_with_user'
        );

        $this->addForeignKey(
            'fk-connect_with_user',
            'projects',
            'connect_with_user',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');

        return false;
    }

}
